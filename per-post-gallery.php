<?php
/*
Plugin Name: Per Post Gallery
Plugin URI: http://kutanaridesain.com/wordpress-plugins/per-post-gallery/
Description: A simple and easy way to set a gallery on the posts!
Author: kutanaridesain
Author URI: http://kutanari.com
Version: 20141104
Text Domain: per-post-gallery
Domain Path: /languages
*/

// configuration
define('PPG_DIR', dirname(__FILE__));
define('PPG_URL', plugins_url( '', __FILE__ ));

require PPG_DIR . '/libs/class.client.php';

if(is_admin()) {
    require PPG_DIR . '/libs/class.admin.php';
}