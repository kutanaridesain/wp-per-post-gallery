<?php 

class PpgClient
{
    public function __construct(){
        add_action( 'wp_enqueue_scripts', array($this, 'register_script') );
        add_action( 'wp_footer', array($this, 'print_script') );
        add_shortcode( 'per-post-gallery', array($this, 'execute_shortcode') );

        do_shortcode('per-post-gallery');
    }

    public function register_script(){
        wp_enqueue_script( 'lightslider', PPG_URL . '/assets/js/jquery.lightSlider.js', array( 'jquery' ), false, false);
        wp_enqueue_script( 'lightGallery', PPG_URL . '/assets/js/lightGallery.js', array( 'lightslider' ), false, false);
        // wp_enqueue_script( 'ppg-front', PPG_URL . '/assets/js/ppg-front.js', array( 'lightGallery' ), false, false);

        wp_enqueue_style( 'lightslider', PPG_URL . '/assets/css/lightSlider.css');
        wp_enqueue_style( 'lightGallery', PPG_URL . '/assets/css/lightGallery.css');
        wp_enqueue_style( 'ppg-front', PPG_URL . '/assets/css/ppg-front.css');
    }

    public function print_script(){
        $current_style = get_option( 'ppg_style' );
        $options_gallery = get_option( 'ppg_options_gallery' );
        $options_slider = get_option( 'ppg_options_slider' );
        $sliderParameter = array(
            'mode'=> $options_slider['mode'],//slide mode : slide and fade, default slide
            'item'=> (int)$options_slider['item'],//number of item displayed on the slider if autoWidth is false, default 4
            'slideMove'=> $options_slider['slideMove'],//number of item to move on every slide if autoWidth false, default 4
            'autoWidth'=> (bool)$options_slider['autoWidth'],//set slider display based on container width, default false, if true above setting will be ignored
            'slideMargin'=> (int)$options_slider['slideMargin'],//space between item on the slider, default 10, aplicable if autoWidth true
            'cssEasing'=> $options_slider['cssEasing'], //ease, linear, ease-in, ease-in-out, cubic-bezier(n,n,n,n)',//the css value for transition timing, default cubic-bezier(1,0,0.1,1)
            'speed'=> (int)$options_slider['speed'], //slide speed in ms', default 500
            'auto'=> (bool)$options_slider['auto'], //auto play the slide, default false
            'loop'=> (bool)$options_slider['loop'], //loop the slide, default false
            // 'slideEndAnimatoin'=> (bool)$options_slider['slideEndAnimatoin'], //default false
            'pause'=> (int)$options_slider['pause'],//delai time in ms, default 1000
            'pager'=> (bool)$options_slider['pager'], //show bullet pager, default true
        );
        
        $galleryParameter = array(
            'mode' => $options_gallery['mode'],//changes mode, slide or fade default slide
            'cssEasing' => $options_gallery['cssEasing'], //default 'cubic-bezier(1,0,0.1,1)',//
            'speed' => (int)$options_gallery['speed'], //speed of transition in ms, default 1000
            'closable' => (bool)$options_gallery['closable'], //close lightbox gallery when clicked on black area, default true
            'loop' => (bool)$options_gallery['loop'], //loop the slide, default false
            'auto' => (bool)$options_gallery['auto'], //autoplay gallery slideshow, default false
            'pause' => (int)$options_gallery['pause'], //delay time, default 2000 in ms
            'escKey' => (bool)$options_gallery['escKey'], //esc key to close the lightbox, default true
            'counter' => (bool)$options_gallery['counter'], //show posision number of image on gallery, default true
            'thumbnail' => (bool)$options_gallery['thumbnail'],//show hide thumbnail on lightbox gallery, default true
            'thumbWidth' => (int)$options_gallery['thumbWidth'],//thumbnail size in px, default 100
            'thumbMargin' => (int)$options_gallery['thumbMargin'],//thumbnail space in px, default 5
        );

        $script = '<script type="application/javascript">';
        $script .= 'jQuery(document).ready(function($) {';
        if($current_style == 'slider' || $current_style == 'grid'){
            $script .= '$(".per-post-gallery .gallery").lightGallery('.json_encode($galleryParameter).');';
        }
        if($current_style == 'slider'){
            $script .= '$(".per-post-gallery .gallery").lightSlider('.json_encode($sliderParameter, JSON_PRETTY_PRINT).');';
        }
        $script .= '});';
        $script .= '</script>';
        echo $script;
    }

    public function execute_shortcode(){
        $ppg_items = get_post_meta( get_the_ID(), 'ppg_items', true );
        ob_start();
        ?>
        <div class="per-post-gallery">
        <ul class="gallery">
            <?php
            foreach ($ppg_items as $ppg_item) {
                $attachment_post_meta = get_post( $ppg_item );
                $attchment_alt = get_post_meta( $ppg_item, '_wp_attachment_image_alt', true );
                $img_alt = empty($attchment_alt) ? $attachment_post_meta->post_title : $attchment_alt ;

                $attachment_url_thumnail = wp_get_attachment_thumb_url( $ppg_item );
                $attachment_url = wp_get_attachment_url( $ppg_item );
            ?>
                <li data-src="<?php echo $attachment_url; ?>"> 
                    <a href="#">
                        <img src="<?php echo $attachment_url_thumnail; ?>" alt="<?php echo $img_alt; ?>" />
                    </a> 
                </li>
            <?php
            }
            ?>
        </ul>
        </div>
        <?php
        return ob_get_clean();
    }
}


new PpgClient;