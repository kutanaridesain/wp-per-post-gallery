<?php
class PpgAdmin
{
    private $styles = array('grid', 'slider');
    private $options;

    public function __construct() {
        add_action( 'admin_enqueue_scripts', array($this, 'print_script') );
        add_action( 'admin_menu', array($this, 'add_page') );
        add_action( 'admin_init', array($this, 'register_setting') );
        add_action( 'add_meta_boxes', array($this, 'register_meta_box') );
        add_action( 'save_post', array($this, 'save_post'), 10, 2 );
    }

    public function ppg_mce_plugin_script($plugin_array){
        $plugin_array['ppg'] = PPG_URL . '/assets/js/ppg_mce_plugin.js';
        return $plugin_array;
    }

    public function register_ppg_button($buttons) {
       array_push($buttons, 'ppg');
       return $buttons;
    }

    public function print_script() {
        wp_enqueue_style('ppgstyle', PPG_URL . '/assets/css/admin-style.css', '', true, 'all' );
        wp_enqueue_script( 'ppgscript', PPG_URL . '/assets/js/ppg.js', array( 'jquery' ), true, true);

        $translationText = array(
            'frameTitle' => __( 'Per Post Gallery', 'per-post-gallery' ),
            'frameButton' => __( 'Submit', 'per-post-gallery' )
        );
        wp_localize_script( 'ppgscript', 'PPG_LOCALIZE', $translationText );
    }

    //section for metabox
    public function register_meta_box() {

        //get current post type
        $cur_post_type = get_post_type();
        //selected post type for ppg plugin
        $ppg_sel_post_type = get_option( 'ppg_selected_post_type' );
        //check if current post type is selected
        if( !isset($ppg_sel_post_type[$cur_post_type]) ){
            return;
        }
        add_meta_box( 'ppg_meta_box', 'Per Post Gallery', array($this, 'create_meta_box'), $cur_post_type, 'side', 'default', array());

        // add shorcode button on editor if metabox is available for current post type
        add_filter('mce_external_plugins', array($this, 'ppg_mce_plugin_script'));
        add_filter('mce_buttons', array($this, 'register_ppg_button'));
    }

    public function create_meta_box($post) {
        wp_nonce_field( 'ppg_meta_box', 'ppg_meta_box_nonce');
        $items = get_post_meta( $post->ID, 'ppg_items', true );
        ?>
        <div id="ppgPreview" class="clearfix">
            <?php
            if($items){
                foreach($items as $item){ 
                    ?>
                    <div class="thumbnail">
                        <?php echo wp_get_attachment_image( $item, 'thumbnail'); ?>
                        <span class="delete-btn">Delete</span>
                        <input type="hidden" name="ppg_items[]" value="<?php echo $item; ?>" />
                    </div>
              <?php };
            }
            ?>
        </div>
        <div>
            <a href="#" id="btnAddImg"><?php _e('Add images', 'per-post-gallery'); ?></a>
        </div>
        <?php
    }

    public function save_post($post_id, $post = false) {
        // check if nonce is set
        if( !isset( $_POST['ppg_meta_box_nonce'] ) ){
            return;
        }

        // verify if nonce is valid
        if( !wp_verify_nonce( $_POST['ppg_meta_box_nonce'], 'ppg_meta_box' ) ){
            return;
        }

        $the_data = $_POST['ppg_items'];
        update_post_meta( $post_id, 'ppg_items', $the_data );
    }
    //end section for metabox

    //section for admin option page
    public function add_page() {
        add_media_page( 'Per Post Gallery', 'Per Post Gallery', 'manage_options', 'perpostgallery', array($this, 'create_page') );
        if ( isset($_REQUEST['option_page']) && $_REQUEST['option_page'] == 'ppg-options-group' ) {
            if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'update'){
                if(is_null($_REQUEST['ppg-options-group']['ppg_selected_post_type'])){
                    delete_option( 'ppg_selected_post_type' );
                }else{
                    update_option( 'ppg_selected_post_type', $_REQUEST['ppg-options-group']['ppg_selected_post_type'] );
                }
                update_option('ppg_style',  $_REQUEST['ppg-options-group']['ppg_style']);
                update_option('ppg_options_gallery',  $_REQUEST['ppg-options-group']['ppg_options_gallery']);
                update_option('ppg_options_slider',  $_REQUEST['ppg-options-group']['ppg_options_slider']);
            }
        }
    }

    public function register_setting(){
        register_setting( 
            'ppg-options-group', 
            'ppg_options' 
        );
        
        add_settings_section( 
            'ppg_setting_page', 
            __( 'Select post or custom post type to assign the gallery and style', 'per-post-gallery' ), 
            '',
            'perpostgallery' 
        );
        
        add_settings_field( 
            'selected_post_type', 
            __('Select Post Type', 'per-post-gallery'), 
            array($this, 'selected_post_type_cb'), 
            'perpostgallery', 
            'ppg_setting_page'
        );

        add_settings_field( 
            'ppg_style', 
            __('Display style', 'per-post-gallery'), 
            array($this, 'style_radio_selection'), 
            'perpostgallery', 
            'ppg_setting_page'
        );

        //gallery and slider settings section
        add_settings_section( 
            'ppg_tab_settings', 
            __( 'Gallery and Slider settings', 'per-post-gallery' ), 
            array($this, 'tab_settings'),
            'perpostgallery' 
        );
    }

    public function style_radio_selection(){
        $current_style = get_option( 'ppg_style' );
        foreach ($this->styles as $style) {
            $active = ( $current_style == $style) ? 'active' : '';
            printf('<div class="custom-radio %s" data-toggle="tooltip" data-placement="top" title="Check this so the gallery will shown in %s mode." data-ppg-style="%s"><div class="radio-icon %s"></div><span>%s</span></div>', $active, $style, $style, $style, ucfirst($style));
        }
        ?>
        <input type="hidden" name="ppg-options-group[ppg_style]" value="<?php echo $current_style; ?>" id="ppg_style">
        <?php
    }

    public function selected_post_type_cb(){
        $builtin_post_types = array('page', 'post');
        $registered_post_types = get_post_types( array('_builtin'=>false), 'names' );
        $post_types = array_merge($builtin_post_types, $registered_post_types);

        foreach ($post_types as $post_type) {
            $post_type_esc = esc_attr($post_type);
            $options = array_values($this->options);

            $checked = isset($this->options[$post_type]) ? 'checked="checked"' : '';
            printf('<input type="checkbox" name="ppg-options-group[ppg_selected_post_type][%s]" %s id="%s" value="%s"><label for="%s">%s</label>', $post_type_esc, $checked, $post_type_esc, $post_type_esc, $post_type_esc, $post_type_esc);
        }
    }

    public function tab_settings()
    {
        //gallery settings
        $options_gallery = get_option( 'ppg_options_gallery' );
        $gal_mode = isset($options_gallery['mode']) ? $options_gallery['mode'] : 'slide';
        $gal_cssEasing = isset($options_gallery['cssEasing']) ? $options_gallery['cssEasing'] : 'cubic-bezier(1,0,0.1,1)';
        $gal_speed = isset($options_gallery['speed']) ? $options_gallery['speed'] : '1000';
        $gal_closable = isset($options_gallery['closable']) ? $options_gallery['closable'] : 1;
        $gal_loop = isset($options_gallery['loop']) ? $options_gallery['loop'] : 0 ;
        $gal_auto = isset($options_gallery['auto']) ? $options_gallery['auto'] : 1;
        $gal_pause = isset($options_gallery['pause']) ? $options_gallery['pause'] : '2000';
        $gal_escKey = isset($options_gallery['escKey']) ? $options_gallery['escKey'] : 1;
        $gal_counter = isset($options_gallery['counter']) ? $options_gallery['counter'] : 1;
        $gal_thumbnail = isset($options_gallery['thumbnail']) ? $options_gallery['thumbnail'] : 1;
        $gal_thumbWidth = isset($options_gallery['thumbWidth']) ? $options_gallery['thumbWidth'] : '100';
        $gal_thumbMargin = isset($options_gallery['thumbMargin']) ? $options_gallery['thumbMargin'] : '5';

        // slider settings
        $options_slider = get_option( 'ppg_options_slider' );
        $slider_item = isset($options_slider['item']) ? $options_slider['item'] : 4; 
        $slider_slideMove = isset($options_slider['slideMove']) ? $options_slider['slideMove'] : 4; 
        $slider_autoWidth = isset($options_slider['autoWidth']) ? $options_slider['autoWidth'] : 0; 
        $slider_slideMargin = isset($options_slider['slideMargin']) ? $options_slider['slideMargin'] : 10; 
        $slider_mode = isset($options_slider['mode']) ? $options_slider['mode'] : 'slide'; 
        $slider_cssEasing = isset($options_slider['cssEasing']) ? $options_slider['cssEasing'] : 'cubic-bezier(1,0,0.1,1)'; 
        $slider_speed = isset($options_slider['speed']) ? $options_slider['speed'] : 500; 
        $slider_auto = isset($options_slider['auto']) ? $options_slider['auto'] : 0; 
        $slider_loop = isset($options_slider['loop']) ? $options_slider['loop'] : 0; 
        $slider_slideEndAnimatoin = isset($options_slider['slideEndAnimatoin']) ? $options_slider['slideEndAnimatoin'] : 0; 
        $slider_pause = isset($options_slider['pause']) ? $options_slider['pause'] : 1000; 
        $slider_thumbItem = isset($options_slider['thumbItem']) ? $options_slider['thumbItem'] : 10; 
        $slider_pager = isset($options_slider['pager']) ? $options_slider['pager'] : 1; 

        $label_value = array('yes'=>1, 'no'=>0);

        ?>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#ppg_gallery" data-toggle="tab"><?php _e('Gallery', 'per-post-gallery'); ?></a></li>
          <li><a href="#ppg_slider" data-toggle="tab"><?php _e('Slider', 'per-post-gallery'); ?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <!-- gallery tab section -->
          <div class="tab-pane fade in active" id="ppg_gallery">
              <table class="form-table">
                  <tr>
                      <th scope="row"><?php _e('Mode', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Animation mode slide or fade', 'per-post-gallery'); ?>">[?]</span></th>
                      <td> 
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][mode]', array('slide'=>'slide', 'fade'=>'fade'), $gal_mode); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('CSS Easing', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('CSS value for animation timing', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <input type="text" name="ppg-options-group[ppg_options_gallery][cssEasing]" value="<?php echo $gal_cssEasing; ?>">
                          <em>[cubic-bezier(1,0,0.1,1)][ease][ease-in][ease-in-out]</em>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Closable modal', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Allows clicks on dimmer to close gallery, if it set to true', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                      <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][closable]', $label_value, $gal_closable); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Speed', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Animation speed in ms', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <input type="text" name="ppg-options-group[ppg_options_gallery][speed]" value="<?php echo $gal_speed; ?>" /> <em>ms</em>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Loop', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Jump to first item on gallery after the end of gallery item', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][loop]', $label_value, $gal_loop); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Autoplay', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Autoplay the gallery slide', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][auto]', $label_value, $gal_auto); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Delay time', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Delay time in millisecond on each galleri image, aplicable if autoplay above is Yes', 'per-post-gallery'); ?>">[?]</span></th>
                      <td><input type="number" value="<?php echo $gal_pause; ?>" name="ppg-options-group[ppg_options_gallery][pause]"> <em>ms</em></td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('ESC to close', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Close the gallery window if ESC key is pressed', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][escKey]', $label_value, $gal_escKey); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Show Counter', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Show information about total image on gallery and current active item posision on screen', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][counter]', $label_value, $gal_counter); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Show thumbnail', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Show/Hide thumbnail button to show gallery thumbnails', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_gallery][thumbnail]', $label_value, $gal_thumbnail); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Thumbnail Width', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Thumbnail size in pixel, if thumbnail is shown', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <input type="number" value="<?php echo $gal_thumbWidth; ?>" name="ppg-options-group[ppg_options_gallery][thumbWidth]"> <em>px</em>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Thumbnail Space', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Space between thumbnail in pixel', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <input type="number" value="<?php echo $gal_thumbMargin; ?>" name="ppg-options-group[ppg_options_gallery][thumbMargin]"> <em>px</em>
                      </td>
                  </tr>
              </table>
          </div>
          <!-- /end of gallery tab section -->
          <!-- slider tab section -->
          <div class="tab-pane fade" id="ppg_slider">
              <table class="form-table">
                  <tr>
                      <th scope="row"><?php _e('Mode', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Animation mode slide or fade', 'per-post-gallery'); ?>">[?]</span></th>
                      <td> 
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_slider][mode]', array('slide'=>'slide', 'fade'=>'fade'), $slider_mode); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Number of item(s)', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Number of items(s) displayed on the slider if Automatic Width setting is No', 'per-post-gallery'); ?>">[?]</span></th>
                      <td> 
                        <input type="number" name="ppg-options-group[ppg_options_slider][item]" value="<?php echo $slider_item; ?>">
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Slide Move', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Number of slider items to move on each sliding', 'per-post-gallery'); ?>">[?]</th>
                      <td>
                        <input type="number" name="ppg-options-group[ppg_options_slider][slideMove]" value="<?php echo $slider_slideMove; ?>">
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Automatic Width', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Set slider width based on container width. PS: if this set to Yes, above settings will be ignored', 'per-post-gallery'); ?>">[?]</th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_slider][autoWidth]', $label_value, $slider_autoWidth); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Slide Space', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Space between item on the slider, if Automatic Width is true', 'per-post-gallery'); ?>">[?]</th>
                      <td>
                        <input type="number" name="ppg-options-group[ppg_options_slider][slideMargin]" value="<?php echo $slider_slideMargin; ?>">
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('CSS Easing', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('CSS value for animation timing', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <input type="text" name="ppg-options-group[ppg_options_slider][cssEasing]" value="<?php echo $slider_cssEasing; ?>">
                          <em>[cubic-bezier(1,0,0.1,1)][ease][ease-in][ease-in-out]</em>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Speed', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Animation speed in ms', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                          <input type="text" name="ppg-options-group[ppg_options_slider][speed]" value="<?php echo $slider_speed; ?>" /> <em>ms</em>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Loop', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Jump to first item on slider after the last item', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_slider][loop]', $label_value, $slider_loop); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Autoplay', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Autoplay the slider', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_slider][auto]', $label_value, $slider_auto); 
                        ?>
                      </td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Delay time', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Delai time in millisecond to sliding', 'per-post-gallery'); ?>">[?]</span></th>
                      <td><input type="number" value="<?php echo $slider_pause; ?>" name="ppg-options-group[ppg_options_slider][pause]"> <em>ms</em></td>
                  </tr>
                  <tr>
                      <th scope="row"><?php _e('Display Pager', 'per-post-gallery'); ?> <span data-toggle="tooltip" data-placement="right" title="<?php _e('Display pager bullets under slider box', 'per-post-gallery'); ?>">[?]</span></th>
                      <td>
                        <?php 
                        $this->generate_radio('ppg-options-group[ppg_options_slider][pager]', $label_value, $slider_pager); 
                        ?>
                      </td>
                  </tr>
              </table>
          </div>
          <!-- /end of slider tab section -->
        </div>
        <?php
    }

    private function generate_radio($id, $label_value, $current_value){
      if (is_array($label_value)) {
        foreach ($label_value as $label => $value) {
          $checked = ($current_value == $value) ? 'checked="checked"' : '' ;
          printf('<label>%s <input type="radio" name="%s" value="%s" %s></label>', ucfirst($label), $id, $value, $checked);
        }
      }
    }

    // private function generate_combobox($id, $){
       
    // }

    public function create_page() {
        if ( !current_user_can( 'manage_options' ) ) {
            wp_die( __( 'You do not have sufficient permission to access this page.' , 'per-post-gallery'), 'per-post-gallery' );
        }

        $this->options = get_option( 'ppg_selected_post_type' );

        ?>
        <div class="wrap" id="ppg_admin_page"><h2><?php _e('Per Post Gallery', 'per-post-gallery'); ?></h2>
        <form method="post" action="options.php">
        <?php 
            if(isset($_REQUEST['settings-updated']) && $_REQUEST['settings-updated'] == true){
                ?><div id="message" class="updated fade"><p><strong><?php _e('changes saved', 'per-post-gallery'); ?></strong></p></div>
                <?php
            }
            settings_fields( 'ppg-options-group' );
            do_settings_sections( 'perpostgallery' );
            submit_button();
        ?>
        </form>
        </div>
    <?php
    }
    //end section for admin option page
}

new PpgAdmin;