(function (){
    tinymce.create('tinymce.plugins.ppg', {
        init : function(ed, url) {
            console.log(url+'/ppg.png');
            ed.addButton('ppg',{
                title:'Add per post gallery shortcode',
                cmd:'ppg_add_shortcode',
                image:url+'/ppg.jpg'
            });

            ed.addCommand('ppg_add_shortcode', function (){
                tinyMCE.activeEditor.selection.setContent('[per-post-gallery]');
            });
        }
    });
 
    // Register plugin
    tinymce.PluginManager.add( 'ppg', tinymce.plugins.ppg );
})();